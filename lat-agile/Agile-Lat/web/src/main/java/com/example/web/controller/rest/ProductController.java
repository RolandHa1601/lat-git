package com.example.web.controller.rest;

import com.blibli.oss.backend.common.helper.ResponseHelper;
import com.blibli.oss.backend.common.model.request.PagingRequest;
import com.blibli.oss.backend.common.model.response.Response;
import com.blibli.oss.backend.reactor.scheduler.SchedulerHelper;
import com.blibli.oss.backend.command.executor.CommandExecutor;
import com.example.command.*;
import com.example.command.model.request.*;
import com.example.model.request.ProductWebRequest;
import com.example.model.response.ProductWebResponse;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import java.util.Collections;

@RestController
@RequestMapping("/product")
public class ProductController implements InitializingBean {

    @Autowired
    protected CommandExecutor executor;

    @Autowired
    private SchedulerHelper schedulerHelper;

    private Scheduler commandScheduler;

    @Override
    public void afterPropertiesSet() throws Exception {
        commandScheduler = schedulerHelper.of("COMMAND");
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Response<String>> insertProduct(
            @RequestBody ProductWebRequest request) {
        InsertProductRequest commandRequest = InsertProductRequest.builder()
                .description(request.getDescription())
                .name(request.getName())
                .stock(request.getStock())
                .photo(request.getPhoto())
                .code(request.getCode())
                .build();

        return executor.execute(InsertProductCommand.class, commandRequest)
                .map(ResponseHelper::ok)
                .subscribeOn(commandScheduler);

    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Response<Page<ProductWebResponse>>> getAllProduct(
//            @ModelAttribute PagingRequest pagingRequest
            @RequestParam(defaultValue = "1") int page,@RequestParam(defaultValue = "10") int itemPerPage
    ) {
        PagingRequest pagingRequest = PagingRequest.builder()
                .page((long) page)
                .itemPerPage((long) itemPerPage)
                .sortBy(Collections.emptyList())
                .build();
        GetAllProductCommandRequest pr = GetAllProductCommandRequest.builder().
                pagingRequest(pagingRequest).build();
        return this.executor.execute(GetAllProductCommand.class,pr)
                .map(ResponseHelper::ok)
                .subscribeOn(commandScheduler);
    }

    @DeleteMapping(value = "/{id}" , produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Response<String>> deleteProduct(@PathVariable("id") int id) {

        DeleteProductCommandRequest request = DeleteProductCommandRequest.builder()
                .id(id)
                .build();

        return executor.execute(DeleteProductCommand.class, request)
                .map(ResponseHelper::ok)
                .subscribeOn(commandScheduler);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Response<String>> editProductById(
            @PathVariable("id") int id,
            @RequestBody ProductWebRequest request) {

        EditProductCommandRequest commandRequest = EditProductCommandRequest.builder()
                .code(request.getCode())
                .id(id)
                .description(request.getDescription())
                .name(request.getName())
                .stock(request.getStock())
                .build();

        return this.executor.execute(EditProductCommand.class, commandRequest)
                .map(ResponseHelper::ok)
                .subscribeOn(commandScheduler);
    }

    @PutMapping(value = "/buy-product/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Response<String>> buyProduct(
            @PathVariable("id") int id,
            @RequestBody ProductWebRequest request) {

        BuyProductCommandRequest commandRequest = BuyProductCommandRequest.builder()
                .id(id)
                .build();

        return this.executor.execute(BuyProductCommand.class, commandRequest)
                .map(ResponseHelper::ok)
                .subscribeOn(commandScheduler);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Response<ProductWebResponse>> getProductDetailById(
            @PathVariable("id") int id) {

        GetProductDetailCommandRequest request = GetProductDetailCommandRequest.builder()
                .id(id)
                .build();

        return executor.execute(GetProductDetailCommand.class, request)
                .map(ResponseHelper::ok)
                .subscribeOn(commandScheduler);
    }

    @GetMapping(value = "/lat-kafka-produce", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Response<String>> latKafkaProduce(
            ) {

        EditProductCommandRequest request = EditProductCommandRequest.builder()
                .build();

        return executor.execute(LatKafkaProducerCommand.class, request)
                .map(ResponseHelper::ok)
                .subscribeOn(commandScheduler);
    }

    @GetMapping(value = "/lat-kafka-consume", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Response<String>> latKafkaConsume(
            ) {

        EditProductCommandRequest request = EditProductCommandRequest.builder()
                .build();

        return executor.execute(LatKafkaConsumerCommand.class, request)
                .map(ResponseHelper::ok)
                .subscribeOn(commandScheduler);
    }



}
