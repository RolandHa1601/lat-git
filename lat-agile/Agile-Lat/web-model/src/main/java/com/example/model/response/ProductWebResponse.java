package com.example.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductWebResponse {

    private String code;
    private int id;
    private String photo;
    private String name;
    private String description;
    private int stock;
}

