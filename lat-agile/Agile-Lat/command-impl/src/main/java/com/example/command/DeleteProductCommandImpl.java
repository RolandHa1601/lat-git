package com.example.command;

import com.example.command.model.request.DeleteProductCommandRequest;
import com.example.command.model.request.ProductNotFoundException;
import com.example.entity.Product;
import com.example.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class DeleteProductCommandImpl implements DeleteProductCommand {

    @Autowired
    private ProductRepository productRepository;


    private String deleteProduct(int id) {
        Product find = productRepository.findById(id);
        if(find == null){
            throw new ProductNotFoundException(id);
        }
        try {
            productRepository.delete(find);
            return "ok";
        }catch (Exception e){
            log.error("product error" + ", with error message {}", e.getMessage());
            throw new ProductNotFoundException(id);
        }
    }

    @Override
    public Mono<String> execute(DeleteProductCommandRequest deleteProductCommandRequest) {
        return Mono.just(deleteProduct(deleteProductCommandRequest.getId()));
    }
}
