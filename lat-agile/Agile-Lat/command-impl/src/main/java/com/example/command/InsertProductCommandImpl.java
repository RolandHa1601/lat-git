package com.example.command;

import com.example.command.model.request.InsertProductRequest;
import com.example.entity.Product;
import com.example.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@Service
public class InsertProductCommandImpl implements InsertProductCommand {

    @Autowired
    ProductRepository productRepository;

    private String saveProduct(InsertProductRequest insertProductRequest) {
//        List<Product> listAll = productRepository.findAll();
//        int size = listAll.size();
//        Product lastProduct = listAll.get(size-1);
//        Integer split = Integer.parseInt(lastProduct.getCode().substring(1));
//        String code = "A" + String.format("%03d",split + 1);
        Product product = Product.builder()
                .description(insertProductRequest.getDescription())
                .code(insertProductRequest.getCode())
                .photo(insertProductRequest.getPhoto())
                .stock(insertProductRequest.getStock())
                .name(insertProductRequest.getName())
                .build();

        try {
            productRepository.save(product);
            return "ok";
        } catch (Exception e) {

            log.error("product error" + ", with error message {}", e.getMessage());
            throw e;
//            return "not ok";
        }
    }

    @Override
    public Mono<String> execute(InsertProductRequest insertProductRequest) {

        return Mono.just(this.saveProduct(insertProductRequest));
    }
}
