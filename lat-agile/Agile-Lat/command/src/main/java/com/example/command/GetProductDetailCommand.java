package com.example.command;

import com.blibli.oss.backend.command.Command;
import com.example.command.model.request.GetProductDetailCommandRequest;
import com.example.command.model.request.InsertProductRequest;
import com.example.model.response.ProductWebResponse;

public interface GetProductDetailCommand extends Command<GetProductDetailCommandRequest, ProductWebResponse> {
}
