package com.example;

import com.example.entity.Product;
import com.example.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
class LoadDatabase {

    @Bean
    public CommandLineRunner initDatabase(ProductRepository repository) {
        return args -> {
//            log.info("Preloading " + repository.save(new Product("Test","code1" ,"photo", "Test_desc-22",15)));
//            log.info("Preloading " + repository.save(new Product("Test_2","code2" ,"photo harusnya","Test_desc_2",5)));
        };
    }


}