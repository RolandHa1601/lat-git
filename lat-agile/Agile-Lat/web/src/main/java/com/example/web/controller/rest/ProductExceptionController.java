package com.example.web.controller.rest;

import com.blibli.oss.backend.common.model.response.Response;
import com.example.command.model.request.ProductNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ProductExceptionController {
//    public ResponseEntity<Object> exception(ProductNotFoundException exception) {
//        return new ResponseEntity<>("Product not found", HttpStatus.NOT_FOUND);
//    }

    //    @ResponseStatus(HttpStatus.OK)
//    @ExceptionHandler(com.blibli.oss.common.error.ValidationException.class)
//    public Response<Object> validationException(com.blibli.oss.common.error.ValidationException e) {
//        getLogger().warn(com.blibli.oss.common.error.ValidationException.class.getName(), e);
//        Response<Object> response = new Response<>();
//        response.setCode(HttpStatus.BAD_REQUEST.value());
//        response.setStatus(HttpStatus.BAD_REQUEST.name());
//        response.setErrors(Errors.from(e.getConstraintViolations()));
//        return response;
//    }
    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity<Response<Object>> notFoundException(ProductNotFoundException ex) {

        Response<Object> body = Response.builder()
                .code(HttpStatus.NOT_FOUND.value())
                .status(HttpStatus.NOT_FOUND.name())
                .data("The requested resource could not be found")
                .build();

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(body);
    }
}

