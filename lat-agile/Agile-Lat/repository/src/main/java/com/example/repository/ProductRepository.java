package com.example.repository;


import com.example.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ProductRepository extends JpaRepository<Product, String> {
    Product findByCode(String code);
    Product findById(int id);
    Integer deleteByCode(String code);
//    Page<Product> findAllSearchAndFilter(
//             Pageable pageable);
}