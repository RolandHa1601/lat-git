package com.example.command;

import com.blibli.oss.backend.common.model.request.PagingRequest;
import com.blibli.oss.backend.common.model.request.SortBy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.domain.*;
import org.springframework.util.CollectionUtils;


/**
 * @author Bryan Arista
 */
public abstract class PagingHelper{

    private PagingHelper() {}

    public static Pageable createPageable(PagingRequest pagingRequest) {

        return PageRequest.of(pagingRequest.getPage().intValue() - 1, pagingRequest.getItemPerPage().intValue(),
                PagingHelper.createSort(pagingRequest.getSortBy()));
    }

    public static Pageable createPageable(Page page) {

        return PageRequest.of(page.getNumber(), page.getSize(), page.getSort());
    }

    public static Sort createSort(List<SortBy> sorts) {

        return Optional.ofNullable(sorts)
//                .filter(CollectionUtils::isEmpty)
                .map(sortList -> Sort.by(PagingHelper.toOrders(sortList)))
                .orElse(null);
    }



    private static Sort.Order toOrder(SortBy sort) {

        return new Sort.Order(Sort.Direction.valueOf(String.valueOf(sort.getDirection()).toUpperCase()), sort.getPropertyName());
    }

    private static List<Sort.Order> toOrders(List<SortBy> sorts) {

        return sorts.stream()
                .map(PagingHelper::toOrder)
                .collect(Collectors.toList());
    }

    public static <T> Page<T> toPage(Collection<T> content, Page page) {

        return new PageImpl<>(new ArrayList<>(content), PagingHelper.createPageable(page), page.getTotalElements());
    }
}

