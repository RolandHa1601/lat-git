package com.example.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;

@Data
@Entity
@Builder
public class Product {

    private @Id @GeneratedValue int id;
    private  String name;
    private  String code;
    private  String photo;
    private String description;
    private int stock;

    Product(){}

    public Product(String name, String code,String photo, String description, int stock) {

        this.name = name;
        this.code = code;
        this.photo = photo;
        this.description = description;
        this.stock = stock;
    }

    public Product(int id,String name, String code,String photo, String description, int stock) {

        this.id = id;
        this.name = name;
        this.code = code;
        this.photo = photo;
        this.description = description;
        this.stock = stock;
    }


}