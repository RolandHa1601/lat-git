package de.vogella.jpa.simple.main;

import de.vogella.jpa.simple.model.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IFooDAO extends JpaRepository<Todo, Long> {
    Todo findByName(String name);
}
