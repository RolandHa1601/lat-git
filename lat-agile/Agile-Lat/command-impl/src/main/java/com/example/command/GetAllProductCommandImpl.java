package com.example.command;

import com.example.command.model.request.GetAllProductCommandRequest;
import com.example.entity.Product;
import com.example.model.response.ProductWebResponse;
import com.example.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class GetAllProductCommandImpl implements GetAllProductCommand {

    @Autowired
    private ProductRepository productRepository;

    private Page<ProductWebResponse> toProductResponse(Page<Product> products) {
//        List<ProductWebResponse> result = new ArrayList<>();
//        for (Product pr:
//            products ) {
//            result.add(toProductWebResponse(pr));
//        }
//        return result;
        return PagingHelper.toPage(toResponseList(products), products);
    }

    private List<ProductWebResponse> toResponseList(Page<Product> productPage) {
        return productPage.getContent().stream()
                .map(this::toProductWebResponse)
                .collect(Collectors.toList());
    }

    public ProductWebResponse toProductWebResponse(Product product) {


        return ProductWebResponse.builder()
                .code(product.getCode())
                .description(product.getDescription())
                .name(product.getName())
                .id(product.getId())
                .photo(product.getPhoto())
                .stock(product.getStock())
                .build();
    }


    private Page<Product> findProductAll(GetAllProductCommandRequest request) {
        try {
            return productRepository.findAll(PagingHelper.createPageable(request.getPagingRequest()));
        } catch (Exception e) {
            log.error("#INTERNAL error when get all product");
            throw e;
        }
    }


    @Override
    public Mono<Page<ProductWebResponse>> execute(GetAllProductCommandRequest request) {
        return Mono.just(request)
                .map(this::findProductAll)
                .map(this::toProductResponse);
    }
}
