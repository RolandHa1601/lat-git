package com.example.command.test;

import com.example.command.EditProductCommandimpl;
import com.example.command.InsertProductCommandImpl;
import com.example.command.model.request.EditProductCommandRequest;
import com.example.command.model.request.InsertProductRequest;
import com.example.entity.Product;
import com.example.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EditProductCommandImplTest {
    private EditProductCommandRequest commandRequest;

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private EditProductCommandimpl command;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void editProduct() {
        commandRequest =
                EditProductCommandRequest.builder().description("desc_edit").name("name_edit")
                        .stock(10).code("A001").id(1).photo("photo").build();

        Product product = Product.builder().name("name_edit").description("desc_edit").stock(10).code("A001").id(1).photo("photo")
                .build();

        when(productRepository.save(product)).thenReturn(product);

        String response = command.execute(commandRequest).block();

        assertEquals("ok", response);

        verify(productRepository).save(product);
    }
}
