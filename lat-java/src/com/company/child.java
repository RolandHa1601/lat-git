package com.company;

public class child extends parent{
    private int attrChildA;
    private String attrChildB;

    public child(int attrParentA, String attrParentB, int attrChildA, String attrChildB) {
        super(attrParentA, attrParentB);
        this.attrChildA = attrChildA;
        this.attrChildB = attrChildB;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((attrChildB == null) ? 0 : attrChildB.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        child other = (child) obj;
        if (attrChildB == null) {
            if (other.attrChildB != null)
                return false;
        } else if (!attrChildB.equals(other.attrChildB))
            return false;
        return true;
    }


    public int tambah(){
        return attrChildA+attrParentA;
    }
    public String tambahString(){
        return attrChildB+attrParentB;
    }
}
