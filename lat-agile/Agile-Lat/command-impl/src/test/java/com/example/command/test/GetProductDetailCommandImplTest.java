package com.example.command.test;

import com.example.command.GetProductDetailCommand;
import com.example.command.GetProductDetailCommandImpl;
import com.example.command.model.request.GetProductDetailCommandRequest;
import com.example.entity.Product;
import com.example.model.response.ProductWebResponse;
import com.example.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class GetProductDetailCommandImplTest {
    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private GetProductDetailCommandImpl command;

    private Product product;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        product = Product.builder()
                .name("name")
                .code("A001")
                .id(1)
                .stock(10)
                .description("desc")
                .build();
    }

    @Test
    public void execute() {

        when(productRepository.findById(1)).thenReturn(product);

        GetProductDetailCommandRequest commandRequest = GetProductDetailCommandRequest.builder()
                .id(1)
                .build();

        ProductWebResponse response = command.execute(commandRequest).block();

        verify(productRepository).findById(1);

        assertEquals("A001", response.getCode());
        assertEquals("name", response.getName());
    }

}