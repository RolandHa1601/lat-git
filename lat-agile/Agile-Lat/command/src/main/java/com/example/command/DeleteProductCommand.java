package com.example.command;

import com.blibli.oss.backend.command.Command;
import com.example.command.model.request.DeleteProductCommandRequest;

public interface DeleteProductCommand extends Command<DeleteProductCommandRequest, String> {
}
