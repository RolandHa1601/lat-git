package com.example.command.test;

import com.example.command.DeleteProductCommandImpl;
import com.example.command.GetAllProductCommandImpl;
import com.example.command.model.request.DeleteProductCommandRequest;
import com.example.command.model.request.InsertProductRequest;
import com.example.entity.Product;
import com.example.model.response.ProductWebResponse;
import com.example.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DeleteProductCommandImplTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private DeleteProductCommandImpl command;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void deleteProduct() {

        int id = 1;
        Product product = new Product("test","A001","photo","desc",10);
        when(productRepository.findById(id)).thenReturn(product);
        DeleteProductCommandRequest commandRequest =
                DeleteProductCommandRequest.builder().id(1).build();

        String response = command.execute(commandRequest).block();

        assertEquals("ok", response);

        verify(productRepository).findById(1);
        verify(productRepository).delete(product);
    }
}
