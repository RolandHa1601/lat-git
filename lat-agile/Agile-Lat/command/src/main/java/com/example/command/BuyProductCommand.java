package com.example.command;

import com.blibli.oss.backend.command.Command;
import com.example.command.model.request.BuyProductCommandRequest;

public interface BuyProductCommand extends Command<BuyProductCommandRequest, String> {
}
