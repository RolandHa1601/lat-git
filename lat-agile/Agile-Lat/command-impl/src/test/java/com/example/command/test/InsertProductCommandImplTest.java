package com.example.command.test;

import com.example.command.InsertProductCommandImpl;
import com.example.command.model.request.InsertProductRequest;
import com.example.entity.Product;
import com.example.model.response.ProductWebResponse;
import com.example.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class InsertProductCommandImplTest {

    private InsertProductRequest commandRequest;

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private InsertProductCommandImpl command;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void insertProduct() {
        commandRequest =
                InsertProductRequest.builder().photo("photo").description("desc").name("name").code("A001")
                        .stock(10).build();

        Product product = Product.builder().name("name").description("desc").stock(10).code("A001")
                .build();
//        List<Product> prs = new ArrayList<Product>();
//        prs.add(new Product("test","A001","desc",10));

//        when(productRepository.findAll()).thenReturn(prs);
        when(productRepository.save(product)).thenReturn(product);

        String response = command.execute(commandRequest).block();

        assertEquals("ok", response);

        verify(productRepository).findAll();
//        verify(productRepository).save(product);
    }
}
