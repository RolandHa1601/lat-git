package com.example.command.model.request;

import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

@Data
@Builder
public class GetProductDetailCommandRequest {
    private int id;
}