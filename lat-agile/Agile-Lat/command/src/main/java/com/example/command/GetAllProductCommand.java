package com.example.command;

import com.blibli.oss.backend.command.Command;
import com.example.command.model.request.GetAllProductCommandRequest;
import com.example.model.response.ProductWebResponse;
import org.springframework.data.domain.Page;

import java.util.List;

public interface GetAllProductCommand extends Command<GetAllProductCommandRequest, Page<ProductWebResponse>> {


}
