package com.example.command.model.request;

import com.blibli.oss.backend.common.model.request.PagingRequest;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

@Data
@Builder
public class GetAllProductCommandRequest {
    private PagingRequest pagingRequest;
}