import Main.FooService;
import Model.Foo;
import Model.MyDictionary;
import Repository.CreateRepo;
import Repository.CreateRepoImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

import static org.junit.Assert.assertEquals;
@RunWith(MockitoJUnitRunner.class)
public class fooSaveTest {
    @Mock
    private CreateRepoImpl repo;
    @InjectMocks
    public FooService service ;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void SaveTest() {
//        CreateRepo repo = Mockito.mock(CreateRepoImpl.class);
        Foo ret = new Foo();
        ret.setSummary("1");
        ret.setDescription("2");
        ret.setId(0);
        when(repo.save("desc","sum")).thenReturn(ret);

        Foo result = service.insert("sum","desc");
        assertEquals("1",result.getSummary());
        verify(repo).save("desc","sum");
    }

    @Test
    public void test() {
//        CreateRepo repo = Mockito.mock(CreateRepoImpl.class);
        List mockList = Mockito.mock(ArrayList.class);
//        when(mockList.add("one")).thenReturn(true);
        boolean result = mockList.add("one");
        Mockito.verify(mockList).add("one");
        Assertions.assertEquals(true, result);
//        assertEquals(0, mockList.size());

//        Mockito.when(mockList.size()).thenReturn(100);
//        assertEquals(100, mockList.size());
    }

    @Test
    public void groupAssertions() {
        int[] numbers = {0, 1, 2, 3, 4};
        Assertions.assertAll("numbers",
                () -> Assertions.assertEquals(numbers[0], 1),
                () -> Assertions.assertEquals(numbers[3], 3),
                () -> Assertions.assertEquals(numbers[4], 1)
        );
    }

    @Spy
    List<String> spiedList = new ArrayList<String>();

    @Test
    public void lat_spy() {
        spiedList.add("one");
        spiedList.add("two");

        Mockito.verify(spiedList).add("one");
        Mockito.verify(spiedList).add("two");

        assertEquals(2, spiedList.size());

        Mockito.doReturn(100).when(spiedList).size();
        assertEquals(100, spiedList.size());
    }

    @Mock
    Map<String, String> wordMap;
    @Spy
    MyDictionary spyDic = new MyDictionary();

    @Test
    public void whenUseInjectMocksAnnotation_thenCorrect() {
        spyDic = Mockito.spy(new MyDictionary(wordMap));
        Mockito.when(wordMap.get("aWord")).thenReturn("aMeaning");

        assertEquals("aMeaning", spyDic.getMeaning("aWord"));
    }

    @Test
    public void whenNotUseCaptorAnnotation_thenCorrect() {
        List mockList = Mockito.mock(List.class);
        ArgumentCaptor<Integer> arg = ArgumentCaptor.forClass(Integer.class);

        mockList.add("two");
        Mockito.verify(mockList).add(arg.capture());

        assertEquals("one", arg.getValue());
    }


//    @Test
//    void trueAssumption() {
//        assumeTrue(5 > 1);
//        assertEquals(5 + 2, 7);
//    }
}
