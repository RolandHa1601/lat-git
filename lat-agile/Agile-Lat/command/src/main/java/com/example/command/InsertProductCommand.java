package com.example.command;

import com.blibli.oss.backend.command.Command;
import com.example.command.model.request.InsertProductRequest;

public interface InsertProductCommand extends Command<InsertProductRequest, String> {

}
