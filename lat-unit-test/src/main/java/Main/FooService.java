package Main;

import Model.Foo;
import Repository.CreateRepo;
import Repository.CreateRepoImpl;
import Repository.iFooDao;

public class FooService {

    CreateRepo repo = new CreateRepoImpl();
    public Foo insert(String summary,String desc){
        Foo saved = new Foo();
        saved.setDescription(desc);
        saved.setSummary(summary);
        repo.save(desc,summary);
        return saved;
    }

}
