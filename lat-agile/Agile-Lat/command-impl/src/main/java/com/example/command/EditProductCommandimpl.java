package com.example.command;

import com.example.command.model.request.EditProductCommandRequest;
import com.example.command.model.request.InsertProductRequest;
import com.example.command.model.request.ProductNotFoundException;
import com.example.entity.Product;
import com.example.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@Component
public class EditProductCommandimpl implements EditProductCommand{

    @Autowired
    ProductRepository productRepository;
    @Override
    public Mono<String> execute(EditProductCommandRequest editProductCommandRequest) {
        return Mono.just(updateProduct(editProductCommandRequest));
    }

    private String updateProduct(EditProductCommandRequest editProductCommandRequest) {
        Product product = Product.builder()
                .description(editProductCommandRequest.getDescription())
                .code(editProductCommandRequest.getCode())
                .id(editProductCommandRequest.getId())
                .photo(editProductCommandRequest.getPhoto())
                .stock(editProductCommandRequest.getStock())
                .name(editProductCommandRequest.getName())
                .build();
        Product result = productRepository.findById(editProductCommandRequest.getId());
        if(result == null){
            throw new ProductNotFoundException(editProductCommandRequest.getId());
        }
        try{
            productRepository.save(product);
            return "ok";
        }
        catch (Exception e){
            log.error("product error" + ", with error message {}", e.getMessage());
            throw e;
        }
    }
}
