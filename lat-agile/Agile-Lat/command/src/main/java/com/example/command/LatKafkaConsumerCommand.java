package com.example.command;

import com.blibli.oss.backend.command.Command;
import com.example.command.model.request.EditProductCommandRequest;

public interface LatKafkaConsumerCommand extends Command<EditProductCommandRequest, String> {

}
