package com.example.command;

import com.example.command.model.request.BuyProductCommandRequest;
import com.example.command.model.request.ProductNotFoundException;
import com.example.entity.Product;
import com.example.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class BuyProductCommandImpl implements BuyProductCommand {

    @Autowired
    private ProductRepository productRepository;

    private String buyProduct(int id) {
        Product find = productRepository.findById(id);
        if(find == null){
            throw new ProductNotFoundException(id);
        }
        find.setStock(find.getStock() - 1);
        try {
            productRepository.save(find);
            return "ok";
        }catch (Exception e){
            log.error("product error" + ", with error message {}", e.getMessage());
            throw new ProductNotFoundException(id);
        }
    }

    @Override
    public Mono<String> execute(BuyProductCommandRequest buyProductCommandRequest) {
        return Mono.just(buyProduct(buyProductCommandRequest.getId()));
    }
}
