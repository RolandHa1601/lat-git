package com.example.command.test;

import com.blibli.oss.backend.common.model.request.PagingRequest;
import com.example.command.GetAllProductCommandImpl;
import com.example.command.model.request.GetAllProductCommandRequest;
import com.example.entity.Product;
import com.example.model.response.ProductWebResponse;
import com.example.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GetAllProductCommandImplTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private GetAllProductCommandImpl command;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllProduct() {

        PagingRequest pagingRequest = PagingRequest.builder()
                .page((long) 1)
                .itemPerPage((long) 10)
                .sortBy(Collections.emptyList())
                .build();
        PageRequest pageable = PageRequest.of(0, 10);
        Product product = new Product("test","A001","photo","desc",10);
        Page<Product> products = new PageImpl<>(Collections.singletonList(product), pageable, 1);
        when(productRepository.findAll(pageable)).thenReturn(products);

        GetAllProductCommandRequest getAllProductCommandRequest = GetAllProductCommandRequest.builder().pagingRequest(pagingRequest).build();

        Page<ProductWebResponse> response = command.execute(getAllProductCommandRequest).block();

        assertEquals("test", response.getContent().get(0).getName());

        verify(productRepository).findAll(pageable);
    }

}
