package com.example.command;

import com.example.command.model.request.EditProductCommandRequest;
import com.example.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Properties;

@Slf4j
@Component
public class LatKafkaProducerCommandImpl implements LatKafkaProducerCommand {

  @Override
  public Mono<String> execute(EditProductCommandRequest editProductCommandRequest) {
    return Mono.just(produceKafka());
  }

  private String produceKafka() {
    Properties properties = new Properties();
    properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG ,"localhost:9092");
    properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG , StringSerializer.class.getName());
    properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());


    KafkaProducer<String,String> producer = new KafkaProducer<String, String>(properties);
    for (int i=0;i<10;i++){
      ProducerRecord <String,String> producerRecord = new ProducerRecord<>("topic-java","Data ke"+i);
      producer.send(producerRecord);
    }
    producer.close();
    return "ok";
  }
}
