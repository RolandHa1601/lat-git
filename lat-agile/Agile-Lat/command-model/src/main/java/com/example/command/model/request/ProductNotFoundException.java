package com.example.command.model.request;


public class ProductNotFoundException extends RuntimeException {

    public ProductNotFoundException(int id) {
        super("Could not find product " + id);
    }
}