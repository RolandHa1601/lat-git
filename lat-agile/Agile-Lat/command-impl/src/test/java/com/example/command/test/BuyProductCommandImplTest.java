package com.example.command.test;

import com.example.command.BuyProductCommandImpl;
import com.example.command.DeleteProductCommandImpl;
import com.example.command.model.request.BuyProductCommandRequest;
import com.example.command.model.request.DeleteProductCommandRequest;
import com.example.entity.Product;
import com.example.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BuyProductCommandImplTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private BuyProductCommandImpl command;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void BuyProduct() {

        int id = 1;
        Product product = Product.builder().name("name").description("desc").stock(10).code("A001").id(1)
                .build();
        when(productRepository.findById(1)).thenReturn(product);
        BuyProductCommandRequest commandRequest =
                BuyProductCommandRequest.builder().id(id).build();

        String response = command.execute(commandRequest).block();

        assertEquals("ok", response);

        verify(productRepository).findById(id);
    }
}