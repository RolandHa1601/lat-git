package com.example.command.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import java.io.File;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InsertProductRequest {

//    @NotBlank(message = "NotBlank")
    private String name;

//    @NotBlank(message = "NotBlank")
    private String description;

    private String photo;

    private String code;


//    @NotBlank(message = "NotBlank")
    private int stock;

}

