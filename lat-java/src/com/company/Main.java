package com.company;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
	// write your code here
        child a = new child(5,"a",3,"b");
        System.out.println(a.tambah());
        parent b = new parent(5,"Z");
        System.out.println(b.attrParentB);


        child z = new child(1,"a",1,"a");
        child x = new child(1,"a",1,"b");
        child w = new child(2,"b",2,"b");
        child k = new child(3,"c",3,"c");
        child y = new child(2,"c",3,"a");

        List<child> iniList = new ArrayList<>();
        iniList.add(z);
        iniList.add(x);
        iniList.add(w);
        iniList.add(k);
        iniList.add(y);
        System.out.println(iniList.size());


        Set<child> iniSet = new HashSet<child>();
        iniSet.add(z);
        iniSet.add(x);
        iniSet.add(w);
        iniSet.add(k);
        iniSet.add(y);
        System.out.println(iniSet.size());
    }
}
