package Repository;

import Model.Foo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface iFooDao extends JpaRepository<Foo, Long>{
    Foo findByName(String name);

}
