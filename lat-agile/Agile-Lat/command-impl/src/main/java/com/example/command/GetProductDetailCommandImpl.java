package com.example.command;

import com.example.command.model.request.GetProductDetailCommandRequest;
import com.example.command.model.request.ProductNotFoundException;
import com.example.entity.Product;
import com.example.model.response.ProductWebResponse;
import com.example.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;


@Slf4j
@Component
public class GetProductDetailCommandImpl implements GetProductDetailCommand {

    @Autowired
    private ProductRepository productRepository;

    private Product findProduct(int id) {
        Product result;
//        try {
             result = productRepository.findById(id);
             if(result == null){
                 throw new ProductNotFoundException(id);
             }
//        } catch (Exception e) {
//            log.error("#INTERNAL error when get t placement with code {}, with error message {}", id, e.getMessage());
//            throw new ProductNotFoundException(id);
//        }
        return result;
    }

    private ProductWebResponse toProductWebResponse(Product product) {


        return ProductWebResponse.builder()
                .code(product.getCode())
                .photo(product.getPhoto())
                .id(product.getId())
                .description(product.getDescription())
                .name(product.getName())
                .stock(product.getStock())
                .build();
    }


    @Override
    public Mono<ProductWebResponse> execute(GetProductDetailCommandRequest getProductDetailCommandRequest) {
        return Mono.just(getProductDetailCommandRequest)
                .map(GetProductDetailCommandRequest::getId)
                .map(this::findProduct)
                .map(this::toProductWebResponse);

    }
}