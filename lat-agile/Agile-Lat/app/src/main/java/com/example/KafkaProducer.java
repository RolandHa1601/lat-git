package com.example;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.boot.SpringApplication;

import java.util.Properties;

public class KafkaProducer {
  public static void main(String[] args) {
    Properties properties = new Properties();
    properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG ,"localhost:9092");
    properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG , StringSerializer.class.getName());
    properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());


    org.apache.kafka.clients.producer.KafkaProducer<String,String>
        producer = new org.apache.kafka.clients.producer.KafkaProducer<String, String>(properties);
    for (int i=0;i<10;i++){
      ProducerRecord<String,String> producerRecord = new ProducerRecord<>("topic-java-lagi","Data ke"+i);
      producer.send(producerRecord);
    }
    producer.close();
  }

}
