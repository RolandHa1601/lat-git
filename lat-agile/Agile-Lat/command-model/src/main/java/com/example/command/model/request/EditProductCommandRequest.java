package com.example.command.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import java.io.File;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EditProductCommandRequest {

    private String name;

    private String description;

    private String photo;

    private int id;

    private String code;

    private int stock;

}

