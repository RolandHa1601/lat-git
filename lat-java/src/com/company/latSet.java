package com.company;

import java.util.HashSet;

public class latSet extends HashSet {
    int id;
    String name;

    public latSet(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
//        return super.equals(o);
        return (o instanceof latSet) && (((latSet) o).name.hashCode() == this.name.hashCode());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
